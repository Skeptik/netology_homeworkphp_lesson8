<?php 
session_start();
checkBlock();

function isPost(){
    return $_SERVER['REQUEST_METHOD'] == 'POST';
}

function getParamPost($name){
    return isset($_POST[$name]) ? $_POST[$name] : null;
}

function login($login, $password){
    $user = getUser($login);
    if (!empty($user) && $user['login'] == $login && $user['password'] == $password) {
        $_SESSION['user'] = $user;
        $_SESSION['password'] = $password;
        return true;
    }else{
    	return false;
    }
}

function getUser($login){
    $users = getUsers();
    foreach ($users as $user) {
        if ($user['login'] == $login) {
            return $user;
        }else{
        	return null;
        }
    }
}

function getUsers(){
	if(file_exists(__DIR__ . '/../data/users.json')){
		$fileData = file_get_contents(__DIR__ . '/../data/users.json');
   		 $users = @json_decode($fileData, true);
   		 if (!$users) {
        	return [];
   		 }else{
    		return $users;
   		 }
	}else{
		echo '<h3><a href="logout.php">Выход</a></h3>';
		echo "<h1>Файл не найден</h1>";
		die;
	}
}

function isAuthorised(){
    if (!empty($_SESSION['user']) && !empty($_SESSION['password'])){
        return true;
    }else{
    	return false;
    }
}

function guest(){
	if(!empty($_SESSION['guest'])){
		return true;
	}else{
		return false;
	}
}

function logGuest($name){
	if($name){
		$_SESSION['guest'] = $name;
		return true;
	}else{
		return false;
	}
}

function logout(){
    session_destroy();
}

function checkCoockie(){
        if ($_COOKIE['check'] < 6){
            return false;
        }else if (($_COOKIE['check']) >= 6){
            return true;
        }
}

function incrCoockie(){
    if (isset($_COOKIE['check'])){
        $coockie = $_COOKIE['check'];
        $coockie++;
        setcookie('check', $coockie);
    }
}

function checkBlock(){
    if(isset($_COOKIE['block'])){
        header("Location: block.php");
        die;
    }
}

?>
