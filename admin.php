﻿<?php 
require_once __DIR__.'/core/function.php';
if (!isAuthorised() && !guest()) {
	$protocol = $_SERVER['SERVER_PROTOCOL'];
    header("$protocol 403 Forbidden");
    echo "<h1>Доступ запрещен, ошибка ".http_response_code(403)."</h1>";
    die;
}
 ?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Тесты</title>
	<style type="text/css">
	   div { 
	    padding: 7px;
	    padding-right: 20px; 
	    border: solid 1px black;
	    font-family: Verdana, Arial, Helvetica, sans-serif; 
	    font-size: 13pt; 
	   	background: #E6E6FA;
	   }
	</style>
</head>
<body>
	<h3><a href="logout.php">Выход</a></h3>
	<div align="center" style="border: solid 1px black;">
	<form enctype="multipart/form-data" method="POST" action="list.php" name="myform">
		<label for="input">Выберите тест.</label>
		<?php if (guest()) {
			echo '<input type="file" name="input" disabled>
			<input type="submit" name="buttonFile" value="Отправить" disabled>';
		}else{
			echo '<input type="file" name="input">
		<input type="submit" name="buttonFile" value="Отправить">';
		} ?>
	</form>
	</div>
</body>
</html>