﻿<?php

// if(isset($_FILES['input']['name']) && !empty($_FILES['input']['name'])){
// 	$fileName = $_FILES['input']['name'];
// 	$host  = $_SERVER['HTTP_HOST'];
// 	$uri   = dirname($_SERVER['PHP_SELF']);
// 	$extra = 'test.php';
// 	header("Location: http://$host$uri/$extra?nameTest=$fileName");
// 	echo $uri;
// }else{
// 	echo "<div align='center'>";
// 	echo "<h1>Файл не был загружен</h1>";
// 	backDownloadFile();
// 	echo "</div>";
// 	exit();
// }
require_once __DIR__.'/core/function.php';
if (!isAuthorised() && !guest()) {
	$protocol = $_SERVER['SERVER_PROTOCOL'];
    header("$protocol 403 Forbidden");
    echo "<h1>Доступ запрещен, ошибка ".http_response_code(403)."</h1>";
    die;
}

if(isset($_FILES['input']['name']) && !empty($_FILES['input']['name'])){
	$fileName = $_FILES['input']['name'];
	$explode = explode('.', $fileName);
	if($explode[1] != 'json'){
		echo "<div align='center'>";
		echo "<h1>Можно загружать только файл с расширением json.</h1>";
		backDownloadFile();
		echo "</div>";
		exit();
	}
	if (!move_uploaded_file($_FILES['input']['tmp_name'], $fileName) && !$_FILES['input']['error'] == UPLOAD_ERR_OK){
		echo "<div align='center'>";
		echo "<h1>Не удалось загрузить файл с тестом.</h1>";
		backDownloadFile();
		echo "</div>";
		exit();
	}
}else{
	echo "<div align='center'>";
	echo "<h1>Файл не был загружен</h1>";
	backDownloadFile();
	echo "</div>";
	exit();
}

function backDownloadFile(){
	echo "<a href='admin.php'>Перейти на страницу для загрузки теста.</a>";
}
?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Document</title>
 	<style type="text/css">
   div { 
    padding: 7px;
    padding-right: 20px; 
    border: solid 1px black;
    font-family: Verdana, Arial, Helvetica, sans-serif; 
    font-size: 13pt; 
   	background: #E6E6FA;
   }
</style>
 </head>
 <body>
 	<h3><a href="logout.php">Выход</a></h3>
 	<div align="center">
 	<ul>
 			<h2>Выполнить тест: <a href="test.php?nameTest=<?= $fileName ?>"><?= $fileName ?></a></h2>	
 	</ul>
 	<h4><a href="delete.php?nameTest=<?= $fileName ?>">Удалить тест</a></h4>
 	</div>
 </body>
 </html>