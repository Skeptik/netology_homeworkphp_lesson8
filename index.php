<?php 
require_once __DIR__.'/core/function.php';

if(isPost()){
    if(isset($_COOKIE['check'])){
        if($_COOKIE['check'] == 11){
            setcookie('block', true, time() + 3600);
            header("Location: block.php");
            die;
        }
    }
    if(login(getParamPost('login'), getParamPost('password'))){
        if(checkCoockie()){
            if($_SESSION['capcha'] == getParamPost('capcha')){
                setcookie('check', 0);
                header("Location: admin.php");
                die;
            }else{
                echo '<center><h1>Вы не верно ввели цифры с картинки</h1></center>';
                incrCoockie();
            }  
        }else{
            setcookie('check', 0);
            header("Location: admin.php");
            die;
        }
    }else{
        if(!isset($_COOKIE['check'])){
            setcookie('check', 0);
        }
            echo '<center><h1>Вы не верно ввели Логин или пароль</h1></center>';
            incrCoockie();
    }
    if(logGuest(getParamPost('name'))){
        setcookie('check', 0);
        header("Location: admin.php");
        die;
    }
}
// setcookie('block', true, time()  -10);
// setcookie('check', $coockie, time()-10);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <title>Авторизация</title>
</head>
<body>
<section id="login">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="form-wrap">
                    <h1>Авторизация</h1>
                    <form method="POST" id="login-form">
                        <div class="form-group">
                            <label for="lg" class="sr-only">Логин</label>
                            <input type="text" placeholder="Логин" name="login" id="lg" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="key" class="sr-only">Пароль</label>
                            <input type="password"  placeholder="Пароль" name="password" id="key" class="form-control">
                        </div>
                        <?php 
                        if(isset($_COOKIE['check'])){
                            if (checkCoockie()){
                                echo '<center><table><tr><left><td><img src="capcha.php"></td></left><td><input type="text" name="capcha" id="lg_3" class="form-control"></td></tr></table></center>';
                            }
                        }
                        ?>
                        <input type="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Войти">
                    </form>
                    <br><br>
                    <form method="POST" id="login-form_2">
                        <div class="form-group">
                            <label for="lg" class="sr-only">Логин</label>
                            <input type="text" placeholder="Введите имя" name="name" id="lg_2" class="form-control">
                        </div>
                        <input type="submit" id="btn-login_2" class="btn btn-custom btn-lg btn-block" value="Войти как гость">
                    </form>

                    <hr>
                </div>
            </div> <!-- /.col-xs-12 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section>
</body>
</html>