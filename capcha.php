<?php 
require_once __DIR__.'/core/function.php';
$path = __DIR__.'/png/pumpkin.png';
 $fontFile = __DIR__.'/font/Alex.ttf';
 if(!file_exists($path)){
 	echo "<h1>Картинка не найдена.</h1>";
 	exit();
 }
 if(!file_exists($fontFile)){
 	echo "<h1>Файл с шрифтом не найден.</h1>";
 	exit();
 }
 $image = imagecreatetruecolor(300, 300);
 $backColor = imagecolorallocate($image, 7, 245, 15);
 $textColor = imagecolorallocate($image, 61, 11, 77);
 $myImage = imagecreatefrompng($path);
 $_SESSION['capcha'] = rand(10000, 99999);
 imagefill($image, 10, 10, $backColor);
 imagecopy($image, $myImage, 10, 0, 0, 0, 280, 282);
 imagettftext($image, 80, 40, 50, 200, $textColor, $fontFile, $_SESSION['capcha']);
 header('Content-Type: image/png');
 imagepng($image);
 imagedestroy($image);
 ?>